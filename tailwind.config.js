/** @type {import('tailwindcss').Config} */
export default {
  content: ['./*.{html,js}'],
  theme: {
    extend: {
      colors: {
        pink: 'hsl(322, 100%, 66%)',
        gray: 'hsl(208, 12%, 55%)',
        'light-pink': 'hsl(321, 100%, 78%)',
        'light-red': 'hsl(0, 100%, 63%)',
        'very-dark-cyan': 'hsl(192, 100%, 9%)',
        'very-pale-blue': 'hsl(207, 100%, 98%)',
        'pink-hover': 'hsl(322, 100%, 78%)',
        'blue-hover': 'hsl(192, 100%, 49%)'
      }
    },
    backgroundImage: {
      'section-mobile-1': 'url("bg-section-top-mobile-1.svg")',
      'section-desktop-1': 'url("bg-section-top-desktop-1.svg")',
      'section-mobile-2': 'url("bg-section-bottom-mobile-1.svg")',
      'section-desktop-2': 'url("bg-section-bottom-desktop-1.svg")'
    },
    fontSize: {
      h1: '3rem',
      h2: '2.5rem',
      h3: '1.5rem',
      h4: '1.25rem'
    },
    lineHeight: {
      h1: '4.5rem',
      h2: '3.75rem',
      h3: '2.25rem',
      h4: '1.875rem'
    },
    fontFamily: {
      sans: ['Open Sans', 'sans-serif'],
      heading: ['Poppins', 'sans-serif']
    },
    boxShadow: {
      pink: '0px 0px 5px rgba(255, 82, 193, 0.218778)',
      btn: '0px 3px 7px rgba(0, 37, 46, 0.223053)',
      'btn-desktop': '0px 6px 13px rgba(0, 37, 46, 0.223053)'
    },
    borderRadius: {
      input: '6px',
      sm: '12px',
      md: '20px',
      lg: '40px'
    },
    screens: {
      xl: '1100px'
    }
  },
  plugins: []
}
